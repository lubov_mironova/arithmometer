package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import sample.animation.Shake;

@SuppressWarnings("unused")
public class Controller {

    public static final String NOT_A_NUMBER = "Введите числа";

    @FXML
    private TextField firstNumberField;

    @FXML
    private TextField secondNumberField;

    @FXML
    private TextField sumField;

    @FXML
    void add(ActionEvent event) {
        try {
            if(firstNumberField.getText().isEmpty() || secondNumberField.getText().isEmpty()) {
                Shake operandShake = (firstNumberField.getText().isEmpty()) ? new Shake(firstNumberField) : new Shake(secondNumberField);
                operandShake.playAnimation();
                return;
            }

            int first = Integer.parseInt(firstNumberField.getText());
            int second = Integer.parseInt(secondNumberField.getText());
            int sum = first + second;
            sumField.setText(String.valueOf(sum));
        } catch (Exception e){
            sumField.setText(NOT_A_NUMBER);
        }
    }

    @FXML
    void clear(ActionEvent event) {
        firstNumberField.clear();
        secondNumberField.clear();
        sumField.clear();
    }

    /**
     * Поймав исключение, выводит сообщение об нечисловых данных в полях ввода
     */
    private void catchException() {
        sumField.setText(NOT_A_NUMBER);
    }
}